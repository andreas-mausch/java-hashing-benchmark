Small benchmarking suite to compare different hashing algorithms.

No scientific claim, just to get a feeling what takes how long.

You can parametrize all of these algorithms. I used the "defaults" for each one.

Except for scrypt, there is an extra run with a smaller N (512) to see how much impact it has.

# Results

i7-8550u

```
$ java -jar target/java-hashing-benchmark-1.0-SNAPSHOT-jar-with-dependencies.jar
[main] INFO de.neonew.hashing.Main - argon2(i=10,m=65536,p=1): Starting with 10 iterations
[main] INFO de.neonew.hashing.Main - argon2(i=10,m=65536,p=1): Benchmarking generateHash() took 7803ms
[main] INFO de.neonew.hashing.Main - argon2(i=10,m=65536,p=1): Benchmarking verifyPassword() took 7678ms
[main] INFO de.neonew.hashing.Main - bcrypt(cost=12): Starting with 10 iterations
[main] INFO de.neonew.hashing.Main - bcrypt(cost=12): Benchmarking generateHash() took 4076ms
[main] INFO de.neonew.hashing.Main - bcrypt(cost=12): Benchmarking verifyPassword() took 4052ms
[main] INFO de.neonew.hashing.Main - PBKDF2: Starting with 10 iterations
[main] INFO de.neonew.hashing.Main - PBKDF2: Benchmarking generateHash() took 129ms
[main] INFO de.neonew.hashing.Main - PBKDF2: Benchmarking verifyPassword() took 17ms
[main] INFO de.neonew.hashing.Main - scrypt(N=512,r=8,p=1): Starting with 10 iterations
[main] INFO de.neonew.hashing.Main - scrypt(N=512,r=8,p=1): Benchmarking generateHash() took 21ms
[main] INFO de.neonew.hashing.Main - scrypt(N=512,r=8,p=1): Benchmarking verifyPassword() took 13ms
[main] INFO de.neonew.hashing.Main - scrypt(N=4096,r=8,p=1): Starting with 10 iterations
[main] INFO de.neonew.hashing.Main - scrypt(N=4096,r=8,p=1): Benchmarking generateHash() took 103ms
[main] INFO de.neonew.hashing.Main - scrypt(N=4096,r=8,p=1): Benchmarking verifyPassword() took 94ms
```

Raspberry Pi 3B

```
$ java -jar java-hashing-benchmark-1.0-SNAPSHOT-jar-with-dependencies.jar
[main] INFO de.neonew.hashing.Main - argon2(i=10,m=65536,p=1): Starting with 10 iterations
[main] INFO de.neonew.hashing.Main - argon2(i=10,m=65536,p=1): Benchmarking generateHash() took 74595ms
[main] INFO de.neonew.hashing.Main - argon2(i=10,m=65536,p=1): Benchmarking verifyPassword() took 74286ms
[main] INFO de.neonew.hashing.Main - bcrypt(cost=12): Starting with 10 iterations
[main] INFO de.neonew.hashing.Main - bcrypt(cost=12): Benchmarking generateHash() took 16753ms
[main] INFO de.neonew.hashing.Main - bcrypt(cost=12): Benchmarking verifyPassword() took 16572ms
[main] INFO de.neonew.hashing.Main - PBKDF2: Starting with 10 iterations
[main] INFO de.neonew.hashing.Main - PBKDF2: Benchmarking generateHash() took 660ms
[main] INFO de.neonew.hashing.Main - PBKDF2: Benchmarking verifyPassword() took 130ms
[main] INFO de.neonew.hashing.Main - scrypt(N=512,r=8,p=1): Starting with 10 iterations
[main] INFO de.neonew.hashing.Main - scrypt(N=512,r=8,p=1): Benchmarking generateHash() took 911ms
[main] INFO de.neonew.hashing.Main - scrypt(N=512,r=8,p=1): Benchmarking verifyPassword() took 304ms
[main] INFO de.neonew.hashing.Main - scrypt(N=4096,r=8,p=1): Starting with 10 iterations
[main] INFO de.neonew.hashing.Main - scrypt(N=4096,r=8,p=1): Benchmarking generateHash() took 2475ms
[main] INFO de.neonew.hashing.Main - scrypt(N=4096,r=8,p=1): Benchmarking verifyPassword() took 2457ms
```

