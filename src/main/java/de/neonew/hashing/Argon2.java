package de.neonew.hashing;

import static de.mkammerer.argon2.Argon2Factory.create;
import static java.lang.String.format;

public class Argon2 extends HashingAlgorithm {

    private static final de.mkammerer.argon2.Argon2 ARGON_2 = create();

    private int i;
    private int m;
    private int p;

    Argon2(int i, int m, int p) {
        super(format("argon2(i=%d,m=%d,p=%d)", i, m, p));
        this.i = i;
        this.m = m;
        this.p = p;
    }

    @Override
    public String generateHash(String password) {
        return ARGON_2.hash(i, m, p, password.toCharArray());
    }

    @Override
    public boolean verifyPassword(String password, String hash) {
        return ARGON_2.verify(hash, password.toCharArray());
    }
}
