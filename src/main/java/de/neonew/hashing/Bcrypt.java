package de.neonew.hashing;

import at.favre.lib.crypto.bcrypt.BCrypt.Hasher;
import at.favre.lib.crypto.bcrypt.BCrypt.Verifyer;

import static at.favre.lib.crypto.bcrypt.BCrypt.verifyer;
import static at.favre.lib.crypto.bcrypt.BCrypt.withDefaults;
import static java.lang.String.format;

public class Bcrypt extends HashingAlgorithm {

    public static final Hasher HASHER = withDefaults();
    public static final Verifyer VERIFYER = verifyer();

    private int cost;

    Bcrypt(int cost) {
        super(format("bcrypt(cost=%d)", cost));
        this.cost = cost;
    }

    @Override
    public String generateHash(String password) {
        return HASHER.hashToString(cost, password.toCharArray());
    }

    @Override
    public boolean verifyPassword(String password, String hash) {
        return VERIFYER.verify(password.toCharArray(), hash).verified;
    }
}
