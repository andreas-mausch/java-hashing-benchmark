package de.neonew.hashing;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.Arrays;
import java.util.List;

import static java.lang.String.format;
import static java.lang.System.currentTimeMillis;
import static java.time.Duration.ofMillis;
import static java.util.UUID.randomUUID;
import static java.util.stream.IntStream.range;

public class Main {
    private static final Logger logger = LoggerFactory.getLogger(Main.class);
    private static final int ITERATIONS = 10;

    public static void main(String[] args) {
        List<? extends HashingAlgorithm> algorithms = Arrays.asList(
                new Argon2(10, 65536, 1),
                new Bcrypt(12),
                new PBKDF2(),
                new Scrypt(512, 8, 1),
                new Scrypt(4096, 8, 1)
        );

        algorithms.forEach(algorithm -> {
            logger.info(format("%s: Starting with %d iterations", algorithm.getName(), ITERATIONS));

            measure(algorithm, "generateHash()", () -> algorithm.generateHash(randomUUID().toString()));

            String password = randomUUID().toString();
            String hash = algorithm.generateHash(password);
            measure(algorithm, "verifyPassword()", () -> {
                if (!algorithm.verifyPassword(password, hash)) {
                    throw new IllegalStateException(("Couldn't verify password"));
                }
            });
        });
    }

    private static void measure(HashingAlgorithm algorithm, String actionName, Runnable action) {
        long before = currentTimeMillis();
        range(0, ITERATIONS).forEach(i -> action.run());
        Duration duration = ofMillis(currentTimeMillis() - before);

        logger.info(format("%s: Benchmarking %s took %dms", algorithm.getName(), actionName, duration.toMillis()));
    }
}
