package de.neonew.hashing;

import de.rtner.security.auth.spi.SimplePBKDF2;

public class PBKDF2 extends HashingAlgorithm {

    private static final SimplePBKDF2 pbkdf2 = new SimplePBKDF2();

    PBKDF2() {
        super("PBKDF2");
    }

    @Override
    public String generateHash(String password) {
        return pbkdf2.deriveKeyFormatted(password);
    }

    @Override
    public boolean verifyPassword(String password, String hash) {
        return pbkdf2.verifyKeyFormatted(hash, password);
    }
}
