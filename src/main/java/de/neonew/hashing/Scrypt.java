package de.neonew.hashing;

import static com.lambdaworks.crypto.SCryptUtil.check;
import static com.lambdaworks.crypto.SCryptUtil.scrypt;
import static java.lang.String.format;

public class Scrypt extends HashingAlgorithm {
    private int N;
    private int r;
    private int p;

    Scrypt(int N, int r, int p) {
        super(format("scrypt(N=%d,r=%d,p=%d)", N, r, p));
        this.N = N;
        this.r = r;
        this.p = p;
    }

    @Override
    public String generateHash(String password) {
        return scrypt(password, N, r, p);
    }

    @Override
    public boolean verifyPassword(String password, String hash) {
        return check(password, hash);
    }
}
