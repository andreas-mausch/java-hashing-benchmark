package de.neonew.hashing;

public abstract class HashingAlgorithm {

    private String name;

    HashingAlgorithm(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public abstract String generateHash(String password);

    public abstract boolean verifyPassword(String password, String hash);
}
